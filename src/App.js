import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Homepage } from './Pages/HomePage';
import { BrowserRouter } from 'react-router-dom';
import { Routers } from './routes/route';




function App() {
  return (

    <BrowserRouter>
      <Routers />
    </BrowserRouter>
  )
}

export default App;
