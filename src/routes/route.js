import { Switch, Route, useLocation } from "react-router-dom";
import { DetailGame } from "../Pages/DetailGames";
import { Homepage } from "../Pages/HomePage";
import { AnimatePresence } from "framer-motion";
export const Routers = () => {
    const location = useLocation()

    return (
        <AnimatePresence exitBeforeEnter initial={false}>
            <Switch key={location.pathname}>
                <Route exact path="/">
                    <Homepage />
                </Route>
                <Route exact path="/games/:idGame">
                    <DetailGame />
                </Route>
            </Switch>
        </AnimatePresence>

    )
}