import React from "react";
import dummy from "../../image/dummy.png"
import "./style.css"
import styled from 'styled-components';
import Rate from 'rc-rate';
import "../GameCard/responsive.css"
import 'rc-rate/assets/index.css';


export const GameCard = (props) => {
    const StyledRate = styled(Rate)`
    &.rc-rate {
      font-size: ${({ size }) => size}px;
    }
  `
    return (
        <div className="card-games text-light my-2">
            <div className="Poster">
                <img src={props.image}></img>
            </div>
            <div className="card-title">
                <h5>{props.title}</h5>
                <StyledRate
                    size="24"
                    disabled="false"
                    value={props.rating} />
            </div>
        </div>
    )

}