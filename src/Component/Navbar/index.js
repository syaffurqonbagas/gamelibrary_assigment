import React from "react"
import { Container, Navbar, Nav } from 'react-bootstrap';
import logo from "../../image/logo.png"

export const NavbarGame = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top my-3"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav >
                        <Nav.Link href="#deets">Games</Nav.Link>
                        <Nav.Link href="#memes">
                            PlatForms
                        </Nav.Link>
                        <Nav.Link href="#memes">
                            List
                        </Nav.Link>
                        <Nav.Link href="#memes">
                            Review
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

    )
}