
import React from "react";
import { Container, Button } from "react-bootstrap";
import profile from "../../image/Avatar.jpg"
import "../UserProfile/style.css"

export const Profile = (props) => {
    const { action, adventure, racing, sport, puzzle, all } = props;

    return (
        <div className="bg-dark text-light py-5">
            <Container>
                <div className="d-flex justify-content-center flex-lg-row flex-sm-column flex-column">
                    <div className="avatar m-auto m-lg-0">
                        <img src={profile}></img>
                    </div>
                    <div className="profile d-flex  justify-content-center">

                        <div className="profile-detail mt-3 ms-4">
                            <h1>Bagas Syaffurqon</h1>
                            <h5>Id: 3334598744</h5>
                        </div>
                    </div>
                    <div className="ms-lg-auto d-flex align-items-center justify-content-center">
                        <div className="mt-3 me-5 d-flex flex-column align-items-center">
                            <h3>102</h3>
                            <h5>Games</h5>
                        </div>
                        <div className="mt-3 me-5 d-flex flex-column align-items-center">
                            <h3>22</h3>
                            <h5>WishList</h5>
                        </div>
                        <div className="mt-3 d-flex flex-column align-items-center">
                            <h3>1702</h3>
                            <h5>Reviews</h5>
                        </div>

                    </div>
                </div>
                <hr className="pt-1" />
                {/* Sort By Genre */}
                <div className="d-flex align-items-center mt-4 flex-wrap">
                    <h5 className="me-5 mt-1">Sort By</h5>
                    <div>
                        <Button onClick={all} className="me-2 my-2" variant="secondary">All</Button>{' '}
                        <Button onClick={action} className="me-2 my-2" variant="secondary">Action</Button>{' '}
                        <Button onClick={adventure} className="me-2 my-2" variant="secondary">Adventure</Button>{' '}
                        <Button onClick={racing} className="me-2 my-2" variant="secondary">Racing</Button>{' '}
                        <Button onClick={sport} className="me-2 my-2" variant="secondary">Sports</Button>{' '}
                        <Button onClick={puzzle} className="me-2 my-2" variant="secondary">Puzzle</Button>{' '}
                    </div>


                </div>
            </Container>
        </div>

    )

}