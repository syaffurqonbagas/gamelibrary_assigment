import React from "react";
import { Container } from "react-bootstrap";
import logo from "../../image/logo.png"
import apple from "../../image/AppStore.png"
import "./style.css"

export const Footer = () => {
    return (
        <div className="bg-dark text-light pt-5 pb-3 d-flex">
            <Container>
                <div>
                    <div className="d-flex flex-lg-row flex-column">
                        <div>
                            <div className="title d-flex align-items-center">
                                <img
                                    src={logo}
                                    width="30"
                                    height="30"
                                    className="d-inline-block align-top my-3"
                                    alt="React Bootstrap logo"
                                />
                                <h3 className="ms-3 mt-2">MyGames</h3>
                            </div>
                            <div className="apple mt-2 ms-lg-5 ms-3">
                                <p className="m-0">Social Network</p>
                                <p>For Video Games Fans</p>
                                <img className="mt-5" src={apple}></img>
                            </div>

                        </div>

                        <div className="ms-lg-auto me-lg-5">
                            <div className="title d-flex align-items-center">
                                <h3 className="ms-3 mt-4">Games</h3>
                            </div>
                            <div className="mt-2 ms-3">
                                <p className="select">Xbox One</p>
                                <p className="select">Playstasion 5</p>
                                <p className="select">Nitendo Switch</p>
                                <p className="select">PC / Steam</p>
                            </div>
                        </div>
                        <div className="me-lg-5">
                            <div className="title d-flex align-items-center">
                                <h3 className="ms-3 mt-4">Platforms</h3>
                            </div>
                            <div className="mt-2 ms-3">
                                <p className="select">Xbox One</p>
                                <p className="select">Playstasion 5</p>
                                <p className="select">Nitendo Switch</p>
                                <p className="select">PC / Steam</p>
                            </div>
                        </div>
                        <div className="me-lg-5">
                            <div className="title d-flex align-items-center">
                                <h3 className="ms-3 mt-4">List</h3>
                            </div>
                            <div className="mt-2 ms-3">
                                <p className="select">Xbox One</p>
                                <p className="select">Playstasion 5</p>
                                <p className="select">Nitendo Switch</p>
                                <p className="select">PC / Steam</p>
                            </div>
                        </div>
                        <div>
                            <div className="title d-flex align-items-center">
                                <h3 className="ms-3 mt-4">Reviews</h3>
                            </div>
                            <div className="mt-2 ms-3">
                                <p className="select">Xbox One</p>
                                <p className="select">Playstasion 5</p>
                                <p className="select">Nitendo Switch</p>
                                <p className="select">PC / Steam</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="mt-5">
                    <hr />
                    <h5 className="text-center mt-2">Copyright © 2000-2021 deMovie.  All Rights Reserved</h5>
                </div>
            </Container>
        </div>
    )
}