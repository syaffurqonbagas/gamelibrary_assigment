import React, { useEffect } from "react";
import { Container } from "react-bootstrap";
import { Footer } from "../../Component/Footer";
import { GameCard } from "../../Component/GameCard";
import { NavbarGame } from "../../Component/Navbar";
import { Pagination } from "../../Component/Pagination";
import { Profile } from "../../Component/UserProfile";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getGames } from "../../redux/action/games";
import { Link } from "react-router-dom";
import { motion } from 'framer-motion';

export const Homepage = () => {
    const dispatch = useDispatch();
    const [currentPage, setCurrentPage] = useState(1);
    const [gamePerPage, setGamePerPage] = useState(20);
    const [genreGame, setGenreGame] = useState("")
    const games = useSelector((state) => state.games.listGames)

    useEffect(() => {
        dispatch(getGames(currentPage, genreGame))
    }, [dispatch, currentPage, genreGame])

    console.log(games)

    console.log(currentPage)

    //change page 
    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    //next and prev page
    const handleNextPage = () => {
        setCurrentPage(currentPage + 1)
    }

    const handlePrevPage = () => {
        setCurrentPage(currentPage - 1)
    }

    //filter genre
    const changeAll = () => {
        setGenreGame("")
        setCurrentPage(1)
    }

    const changeAction = () => {
        setGenreGame("action")
        setCurrentPage(1)
    }

    const changeAdventure = () => {
        setGenreGame("adventure")
        setCurrentPage(1)
    }

    const changeRacing = () => {
        setGenreGame("racing")
        setCurrentPage(1)
    }

    const changeSport = () => {
        setGenreGame("sports")
        setCurrentPage(1)
    }

    const changePuzzle = () => {
        setGenreGame("puzzle")
        setCurrentPage(1)
    }

    const pageVariants = {
        initial: {
            opacity: 0,
        },
        in: {
            opacity: 1,
        },
        out: {
            opacity: 0,
        },
    }

    return (
        <>
            <motion.div
                initial="initial"
                animate="in"
                exit="out"
                variants={pageVariants}
            >
                <NavbarGame />
                <Profile
                    all={changeAll}
                    action={changeAction}
                    adventure={changeAdventure}
                    racing={changeRacing}
                    sport={changeSport}
                    puzzle={changePuzzle}
                />
                <div className="bg-dark pb-5">
                    <Container>
                        <div className="d-flex justify-content-lg-between justify-content-md-around justify-content-center 
                    flex-wrap">
                            {games?.length > 0 && games?.map((game, id) => (
                                <Link
                                    style={{ textDecoration: "none" }}
                                    to={`/games/${game.id}`} ><GameCard title={game.name} image={game.background_image} rating={game.rating} /></Link>
                            ))}
                        </div>
                        <div className="d-flex justify-content-center py-5 mt-5">
                            <Pagination
                                postPerPage={gamePerPage}
                                totalPost={100}
                                paginate={paginate}
                                currentPage={currentPage}
                                handleNextPage={handleNextPage}
                                handlePrevPage={handlePrevPage} />
                        </div>
                    </Container>
                </div>
                <Footer />
            </motion.div>
        </>
    )
}
