import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import './style.css'
import hero from "../../image/Edu1.png"
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { getGames, getGamesId } from "../../redux/action/games";
import styled from 'styled-components';
import Rate from 'rc-rate';
import 'rc-rate/assets/index.css';
import { Footer } from "../../Component/Footer";
import { Link } from "react-router-dom";
import { motion } from "framer-motion"

export const DetailGame = () => {
    const dispatch = useDispatch()
    let { idGame } = useParams()
    const games = useSelector((state) => state.games.gamesbyid)
    const [gamesid, setGamesId] = useState([]);

    useEffect(() => {
        dispatch(getGamesId(idGame))
    }, [dispatch, idGame])

    useEffect(() => {
        setGamesId(games)
    }, [getGames])

    console.log("yuhu klkl", gamesid)

    const pageVariants = {
        initial: {
            opacity: 0,
        },
        in: {
            opacity: 1,
        },
        out: {
            opacity: 0,
        },
    }


    const StyledRate = styled(Rate)`
    &.rc-rate {
      font-size: ${({ size }) => size}px;
    }
  `

    return (
        <motion.div initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}>


            <div className="bg-dark text-light">
                <div className='hero d-flex justify-content-center text-light'>
                    <div className="image-hero d-flex flex-column justify-content-between" style={{ backgroundImage: `url(${games.background_image})` }}>
                        <div className="title-detail p-5">
                            <div className="container d-flex justify-content-end">
                                <Link
                                    style={{ textDecoration: "none", color: "white" }}
                                    to="/"
                                > <h3 className="close">X</h3></Link>
                            </div>
                        </div>
                        <div className="title-detail-bot p-4">
                            <h1 className="container">{games.name}</h1>
                        </div>
                    </div>
                </div>
                <div className="p-5 bg-dark">
                    <Container>
                        <div className="d-flex align-items-center flex-column flex-lg-row">
                            <StyledRate
                                size="28"
                                disabled="false"
                                value={games.rating} />
                            <h5 className="mt-2 ms-lg-2">{games.reviews_count} Reviews</h5>
                            <div className="ms-lg-auto d-flex">

                            </div>
                        </div>
                        <div className="d-flex align-items-center flex-column flex-lg-row text-justify">
                            {games.description}
                        </div>
                    </Container>
                </div>
                <Footer />
            </div>
        </motion.div>
    )
}