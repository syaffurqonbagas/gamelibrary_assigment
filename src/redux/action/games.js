import { GET_ALL_GAMES_BEGIN, GET_ID_GAMES_BEGIN } from "./type"

export const getGames = (page, genre = "") => {
    return {
        type: GET_ALL_GAMES_BEGIN,
        page,
        genre
    }
}

export const getGamesId = (id) => {
    return {
        type: GET_ID_GAMES_BEGIN,
        id
    }
}