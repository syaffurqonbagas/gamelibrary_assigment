import { GET_ALL_GAMES_BEGIN, GET_ALL_GAMES_FAIL, GET_ALL_GAMES_SUCCESS, GET_ID_GAMES_BEGIN, GET_ID_GAMES_FAIL, GET_ID_GAMES_SUCCESS } from "../action/type";


const intialState = {
    listGames: [],
    gamesbyid: [],
    loading: false,
    error: null
}

const games = (state = intialState, action) => {
    const { type, payload, error } = action;
    switch (type) {
        case GET_ALL_GAMES_BEGIN:
            return {
                ...state,
                loading: true,
            }
        case GET_ALL_GAMES_SUCCESS:
            return {
                ...state,
                listGames: payload,
                error: null,
            }
        case GET_ALL_GAMES_FAIL:
            return {
                ...state,
                error: error,
            }
        case GET_ID_GAMES_BEGIN:
            return {
                ...state,
                loading: true,
            }
        case GET_ID_GAMES_SUCCESS:
            return {
                ...state,
                gamesbyid: payload,
                error: null
            }
        case GET_ID_GAMES_FAIL:
            return {
                ...state,
                error: error,
            }
        default:
            return {
                ...state
            }
    }
}

export default games;
