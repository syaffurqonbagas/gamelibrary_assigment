import { all } from "@redux-saga/core/effects";
import { watchGetGames, watchGetGamesById } from "./games";

export default function* rootSaga() {
    yield all([
        watchGetGames(),
        watchGetGamesById()
    ])
}