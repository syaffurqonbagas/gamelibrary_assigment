import axios from "axios"
import { GET_ALL_GAMES_BEGIN, GET_ALL_GAMES_FAIL, GET_ALL_GAMES_SUCCESS, GET_ID_GAMES_BEGIN, GET_ID_GAMES_FAIL, GET_ID_GAMES_SUCCESS } from "../action/type"
import { put, takeEvery } from "@redux-saga/core/effects";

function* getGames(actions) {
    const { page, genre } = actions
    let url = ""
    if (genre === "") {
        url = `https://api.rawg.io/api/games?key=42bdcd2b52e64b3491718d9ab7ad7a31&page=${page}`
    } else {
        url = `https://api.rawg.io/api/games?key=42bdcd2b52e64b3491718d9ab7ad7a31&page=${page}&genres=${genre}`
    }
    try {
        const res = yield axios.get(url)
        yield put({
            type: GET_ALL_GAMES_SUCCESS,
            payload: res.data.results
        })

    } catch (error) {
        yield put({
            type: GET_ALL_GAMES_FAIL,
            error: error,
        })
    }
}

function* getGamesByid(action) {
    const { id } = action
    yield console.log("fungsi masuk")
    try {
        const res = yield axios.get(`https://api.rawg.io/api/games/${id}?key=42bdcd2b52e64b3491718d9ab7ad7a31`)
        yield put({
            type: GET_ID_GAMES_SUCCESS,
            payload: res.data
        })
    } catch (error) {
        yield put({
            type: GET_ID_GAMES_FAIL,
            error: error
        })
    }
}

export function* watchGetGames() {
    yield takeEvery(GET_ALL_GAMES_BEGIN, getGames)
}

export function* watchGetGamesById() {
    yield takeEvery(GET_ID_GAMES_BEGIN, getGamesByid)
}