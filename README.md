Project Name & Pitch:

BLibrary

An application used to show list of the games
This project is currently in development. Users can filter games by genres. Functionality to add game in wishlist is in progress.

-----------------------------------------------------------------------------------------------------------------------------------

Clone down this repository. You will need node and npm installed globally on your machine.

Installation:

npm install

To Run Test Suite:

npm test

To Start Server:

npm start

To Visit App with local Server:

localhost:3000

To Visit App Deploy:

https://git.heroku.com/blibrary.git

-----------------------------------------------------------------------------------------------------------------------------------

Reflection:


This is a 4 day project done for individual project Glints Academy. This project is a website that contains a game directory that displays a collection of games that have filter features based on game genres.

I am very happy to work on this project, hopefully this project can get maximum results.

